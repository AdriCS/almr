#alias updatePrey='sudo npm update -g prey --loglevel=verbose && sudo -K'
alias update='sudo apt update && sudo apt upgrade && sudo -K && pip freeze --local | grep -v "^\-e" | cut -d = -f 1  | xargs -n1 pip install -U'
alias lynx='lynx -cfg=~/.lynx.cfg "$1"'
