#
# Beginning of Git functions
#

function ggrafo {
#  echo -e '\E[34;47m** OPTIONAL: --oneline';   # Blue
#  tput sgr0                               # Reset colors to "normal."


  git log --graph --all --decorate
}

function gunmerged {
# Commits in branch $1 that aren't merged in branch $2
    git log $1 ^$2 --no-merges --oneline
}

function gnonshared {
# Will give a list of any non-shared between the branches $1 and $2
#    --left-right -> will show to which branch each commit comes from:
#       Ex: master...devel    -> master is LEFT, devel is RIGHT
#            > 0001asd commit1         ->  THIS COMES FROM devel
#            < abcdwew commitXYZ       ->  THIS COMES FROM master
#
#   $1..$2   -> Shows the commits that are in $2 only.
#   $1...$2  -> it will filter out all of the commits that both $1 and $2 share,
#               thus only showing the commits that they don't both share.
   
    local OPTIND # so we can call 'nonshared' several times with diff params.
    local readonly DOTS="..."
    printf -v USAGE  'USAGE: nonshared [-a] branch1 branch2\n
    *) -a: list the commits that are in branch2 only.\n
       If not specified (DEFAULT), it will show the commits that they '"don't"' both share'
                     
    while getopts ":a" opt; do
        case $opt in
            a)
                DOTS=".."
                shift $((OPTIND - 1))  # shift one
                ;;
            ?)
                echo "Invalid option: -$OPTARG"
                echo "$USAGE"  # with double quotes to use the newlines!
                return 1
                ;;
        esac
    done
   
    git log --graph --decorate --left-right --cherry-pick --oneline "$1$DOTS$2"
}

function glog {
  local readonly number_of_logs=${1:-"--full-history"}
  git log --pretty=format:'%C(yellow)%h %Cred%ad %Cblue%an%Cgreen%d %Creset%s%n' --date=short "$number_of_logs"
}

function gsearchlog {
  git log --pretty=format:'%C(yellow)%h %Cred%ad %Cblue%an%Cgreen%d %Creset%s%n%n' \
    --date=short --regexp-ignore-case --grep=$1
}

function gcompareFile {
    local OPTIND # so we can call 'nonshared' several times with diff params.
   
    printf -v HELP 'Compares a file between two commits.
                    * compareFile [-h] a b c\n
                    -- a -> Commit #1\n
                    -- b -> Commit #2\n
                    -- c -> File to compare.\n
                    -- -h -> OPTIONAL. Shows this help.'
                   
    while getopts ":h" opt; do
        case $opt in
            h)
                echo "$HELP"
                shift $((OPTIND - 1))  # shift one
                return 0
                ;;
            ?)
                echo "Invalid option: -$OPTARG"
                echo "Available option: -h"
                return 1
                ;;
        esac
    done

   git diff $1..$2 -- $3
}

#
# End of Git functions
#

# 
function scan() {
    local -r maxSize="2560M"
	clamscan -irv --bytecode-timeout=190000 --max-filesize="$maxSize" --max-scansize="$maxSize" "$@"
}

