[![pipeline status](https://gitlab.com/AdriCS/almr/badges/master/pipeline.svg)](https://gitlab.com/AdriCS/almr/-/commits/master)
[![Latest Release](https://gitlab.com/AdriCS/almr/-/badges/release.svg)](https://gitlab.com/AdriCS/almr/-/releases)

<h1> Automation for Linux Master Race

Aren't you tired of always having to install and configure everything by hand?

![](.images/glmr.png)
