FROM debian:latest

LABEL description="Dummy image for Ansible testing."

ARG USERNAME
ARG PASSWD
ARG SALT="Q9"
# Trick for Ansible when running in container
ENV USER $USERNAME
RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y curl \
					   git \
					   gcc \
					   g++ \
					   libffi-dev \
					   libssl-dev \
					   openssh-client \
					   openssh-server \
					   python \
					   python-dev \
					   python-setuptools \
					   sudo

RUN easy_install pip && \
	pip install ansible \
				jmespath

RUN useradd -ms /bin/bash -p $(perl -e "print crypt($PASSWD, $SALT)") -G sudo $USERNAME && \
	echo "$USERNAME ALL=(ALL) ALL" >> /etc/sudoers.d/dummy && \
	visudo -c -f /etc/sudoers.d/dummy

USER $USERNAME
WORKDIR /home/$USERNAME/almr

CMD /bin/bash

