#!/bin/bash

readonly scriptPath=$(dirname `readlink -fn -- "$0"`)
readonly ERROR=-1

exitIfError() {
	local lastReturnCode="$1"

	if [[ "$lastReturnCode" -ne 0 ]]
	then
		printf "%s\n\n" "ERROR!"
		exit "$ERROR"
	fi
}

resetSudoTimestamp() {
	sudo -k
}

## Independently of the result, cancel the sudo timestamp
trap resetSudoTimestamp EXIT

## We require sudo!
#if [[ "$EUID" -ne 0 ]]
#then
#	printf "%s\n\n" "This script MUST be executed with SUDO rights."
#	exit "$ERROR"
#fi

## Get the $HOME dir of the user who invoked this script.
## Since we invoked it with sudo, $HOME will point to '/root/'
#USER_HOME=$(getent passwd $SUDO_USER | cut -d: -f6)
#USER_GROUP=$(getent group $SUDO_GID | cut -d: -f1)
#
#printf "** %s -- %s - %s - %s\n" "$scriptPath" "$USER_HOME" "$USER_GROUP" "$ANSIBLE_DIR"
# Pre-requisites
#
apt-get install -y curl \
                   gcc \
                   g++ \
                   openssh-server \
                   openssh-client \
                   python \
                   python-dev \
                   python-setuptools \
                   libffi-dev \
                   libssl-dev
exitIfError "$?"

apt install python-pip python3-pip
exitIfError "$?"

echo "INSTALLING ANSIBLE DEPS"
pip install paramiko \
            PyYAML \
      		markupsafe \
      		Jinja2 \
			jmespath \
      		httplib2 \
      		six
exitIfError "$?"

# Get ansible in a way that we don't have to source it everytime...
yes | pip install ansible
exitIfError "$?"

#Test it!
ansible local -m ping --connection=local

