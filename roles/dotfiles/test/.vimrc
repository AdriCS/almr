"
" Beginning of Vundle config
"
set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim

call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

"
" My plugins
"

" Full path fuzzy file, buffer, mru, tag, ... finder for Vim
Plugin 'ctrlpvim/ctrlp.vim'

" File explorer
Plugin 'scrooloose/nerdtree'
Plugin 'Xuyuanp/nerdtree-git-plugin'

Plugin 'scrooloose/nerdcommenter'

" Status line
Plugin 'bling/vim-airline'
Plugin 'vim-airline/vim-airline-themes'

" Colors!
Plugin 'altercation/Vim-colors-solarized'

Plugin 'Valloric/YouCompleteMe'

Plugin 'vim-latex/vim-latex'

" Buffer explorer
Plugin 'jeetsukumaran/vim-buffergator'

"
" End of 'My plugins'
"

" All of your Plugins must be added before the following line
call vundle#end()            " required

filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line

"
" End of Vundle config
"

let mapleader=","
" Write buffer only if it has been modified
nnoremap <Leader>s :update<CR>
" Exit all
nnoremap <Leader>q :qa<CR>
" Toggle NERDTree
nnoremap <C-n> :NERDTreeToggle<CR>

"
" NERDTree options
"
let NERDTreeShowHidden=1

"
" YCM options
"
" Close the preview window after a suggestion is accepted
let g:ycm_autoclose_preview_window_after_completion = 1

"
" Syntax
"
syntax enable

"
" Color scheme
"
"set background=light
"colorscheme solarized

"
" Tabs
"
set tabstop=4
set softtabstop=4
"set expandtab

"
" UI config
"
set number
set ruler
set showcmd              " show the last command inserted

" Triger `autoread` when files changes on disk:
"   - BASED ON: https://unix.stackexchange.com/questions/149209/refresh-changed-content-of-file-opened-in-vim/383044#383044
"   - With info from:
"       - https://unix.stackexchange.com/questions/149209/refresh-changed-content-of-file-opened-in-vim/383044#383044
"   -   - https://vi.stackexchange.com/questions/13692/prevent-focusgained-autocmd-running-in-command-line-editing-mode
set autoread             "Reload files changed outside vim
autocmd FocusGained,BufEnter,CursorHold,CursorHoldI * if mode() != 'c' | checktime | endif
" " Notification after file change
" " https://vi.stackexchange.com/questions/13091/autocmd-event-for-autoread
autocmd FileChangedShellPost *
  \ echohl WarningMsg | echo "File changed on disk. Buffer reloaded." | echohl  None
"
"

"set cursorline          " Highlight the entire line where the cursor is
filetype indent on
set wildmenu             " Graphical menu of all of the autocomplete targets
set lazyredraw
set showmatch
set laststatus=2         " Show status bar - For Airline

"
" Search
"
set incsearch           " search as characters are entered
set hlsearch            " highlight matches
" turn off search highlight
" nnoremap <leader><space> :nohlsearch<CR>

"
" Wrap
"
set textwidth=80
set wrapmargin=2
" To show a grey vertical guide at 80 columns.
highlight ColorColumn ctermbg=lightgrey guibg=lightgrey
set colorcolumn=80

" highlight all characters past 80 columns.
"noremap <Leader>h :call<SID>LongLineHLToggle()<cr>
"hi OverLength ctermbg=none cterm=none
"match OverLength /\%>80v/
"fun! s:LongLineHLToggle()
"  if !exists('w:longlinehl')
"    let w:longlinehl = matchadd('ErrorMsg', '.\%>80v', 0)
"    echo "Long lines highlighted"
"  else
"    call matchdelete(w:longlinehl)
"	unl w:longlinehl
"	echo "Long lines unhighlighted"
"  endif
"endfunction


"
" Latex
"
let g:tex_flavor='latex'

"
" Airline theme
"
"let g:airline_theme='solarized'

"
" -----------------------------------------------
"
